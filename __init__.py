#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Dispatcher, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup

from telegram.messageentity import MessageEntity
from queue import Queue

import logging
from subprocess import Popen, PIPE
from optparse import OptionParser
import os

from fortunebr_bot.config import Config
import fortunebr_bot.fortune as fortune

config=Config()

def list_get(l, idx, default=None):
	try:
		return l[idx]
	except IndexError:
		return default

def is_filename_in(base_dir, filename):
	here=os.path.abspath(base_dir)
	there=os.path.abspath(filename)
	return there.startswith(here)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
		level=logging.INFO)

logger=logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
	"""Send a message when the command /start is issued."""
	update.message.reply_text('Frases famosas em portugues...')


def help(bot, update):
	"""Send a message when the command /help is issued."""
	ret='''Digite o comando "/fortune" ou "/fortune@fortunebr_bot" em um grupo com o bot.

exemplos de fortunes dispovineis:
'''
	for f in fortune.get_valid_fortune_from_dir(config.FORTUNES_DIR):
		ret+=os.path.basename(f)+"\n"
	update.message.reply_text(ret)

def error(bot, update, error):
	"""Log Errors caused by Updates."""
	logger.warning('Update "%s" caused error "%s"', update, error)

def reply_fortune(bot, update, fortune_file=None):
	ret=""
	if not fortune_file:
		ret+=fortune.fortune_from_file(config.FORTUNES_DIR+"/brasil")
	else:
		fortune_file=config.FORTUNES_DIR+"/"+fortune_file
		if is_filename_in(config.FORTUNES_DIR, fortune_file) and os.path.exists(fortune_file):
			ret+=fortune.fortune_from_file(fortune_file)
		if not ret:
			ret+="Nenhum dicionario de fortune encontrado veja o /help ou tente os dicionarios:\n"
			for f in fortune.get_valid_fortune_from_dir(config.FORTUNES_DIR):
				ret+=os.path.basename(f)+"\n"
	logger.warning('Fortune call %s', ret )
	update.message.reply_text(ret)

def command_fortune(bot, update):
	msg=update.message.text.split(' ')
	fortune_file=list_get(msg, 1)

	reply_fortune(bot, update, fortune_file)

def dm_fortune(bot, update):
	msg=update.message.text.split(' ')
	fortune_file=list_get(msg, 0)

	reply_fortune(bot, update, fortune_file)

def build_menu(
		buttons,
		n_cols,
		header_buttons=None,
		footer_buttons=None
	):
	menu=[buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
	if header_buttons:
		menu.insert(0, header_buttons)
	if footer_buttons:
		menu.append(footer_buttons)
	return menu

def choose_fortune(bot, update):
	message=update.message

	fortune_list=[]
	for f in fortune.get_valid_fortune_from_dir(config.FORTUNES_DIR):
		fortune_list.append(os.path.basename(f))

	button_list=[]
	for f in fortune_list:
		data="/fortune@"+config.TELEGRAM_BOT_USERNAME+" "+f
		button_list.append(KeyboardButton(data, callback_data=data))

	reply_markup=ReplyKeyboardMarkup(build_menu(button_list, n_cols=2))
	bot.send_message(
		text="Escolha um fortune",
		message_id=message.message_id,
		chat_id=message.chat_id,
		reply_markup=reply_markup
	)

def mention_fortune(bot, update, args=None):
	msg=update.message.text.split(' ')
	if msg[0]=="@"+config.TELEGRAM_BOT_USERNAME:
		fortune_file=list_get(msg, 1)

		reply_fortune(bot, update, fortune_file)

def create_from_bot(bot):
	return main(bot)

def main(bot=None):
	parser=OptionParser()
	#parser = OptionParser(usage=usag)
	parser.add_option(
		"-t", "--token-telegram",
		help=("Token recived from FatherBot")
	)
	parser.add_option(
		"-u", "--bot-username",
		help=("Username of bot")
	)
	parser.add_option(
		"-d", "--fortunes-dir",
		help=("Directory with fortune files")
	)

	options, args = parser.parse_args()
	
	if options.token_telegram is not None:
		#print("token set: {}".format(options.token_telegram))
		config.TELEGRAM_BOT_TOKEN=options.token_telegram
	if options.bot_username is not None:
		config.TELEGRAM_BOT_USERNAME=options.bot_username
	if options.fortunes_dir is not None:
		config.FORTUNES_DIR=options.fortunes_dir

	if bot:
		update_queue=Queue()
		dp=Dispatcher(bot, update_queue)
		config.TELEGRAM_BOT_USERNAME=bot.username
	else:
		# Create the EventHandler and pass it your bot's token.
		#print("token set: {}".format(TELEGRAM_BOT_TOKEN))
		updater=Updater(config.TELEGRAM_BOT_TOKEN)
		# Get the dispatcher to register handlers
		dp=updater.dispatcher
		config.TELEGRAM_BOT_USERNAME=updater.bot.username


	# on different commands - answer in Telegram
	dp.add_handler(CommandHandler("start", start))
	dp.add_handler(CommandHandler("help", help))
	dp.add_handler(CommandHandler("fortune", command_fortune))
	dp.add_handler(CommandHandler("escolha", choose_fortune))

	# any mention on start of message
	dp.add_handler(MessageHandler(Filters.text & Filters.entity(MessageEntity.MENTION), mention_fortune))
	# message is not a group chat and text to bot
	dp.add_handler(MessageHandler(Filters.text & ~Filters.group, dm_fortune))

	# log all errors
	dp.add_error_handler(error)

	if os.environ.get('BOT_WEBHOOK_ENABLE', False):
		# Start the Bot
		updater.start_webhook(
			listen="0.0.0.0",
			port=int(os.environ.get('BOT_WEBHOOK_PORT', '8080')),
			url_path=config.TELEGRAM_BOT_TOKEN,
			webhook_url=os.environ.get('BOT_WEBHOOK_BASE_URL')+config.TELEGRAM_BOT_TOKEN
		)
		updater.bot.setWebhook(os.environ.get('BOT_WEBHOOK_BASE_URL')+config.TELEGRAM_BOT_TOKEN)

		# Run the bot until you press Ctrl-C or the process receives SIGINT,
		# SIGTERM or SIGABRT. This should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		updater.idle()
	elif not bot:
		# Start the Bot
		updater.start_polling()

		# Run the bot until you press Ctrl-C or the process receives SIGINT,
		# SIGTERM or SIGABRT. This should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		updater.idle()
	else:
		class dummy():
			def __init__(self, dp,config):
				self.dispatcher=dp
				self.TELEGRAM_BOT_USERNAME=config.TELEGRAM_BOT_USERNAME

		return dummy(dp, config)


if __name__ == '__main__':
	main()

