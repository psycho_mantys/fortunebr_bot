from os import environ

class Config(object):
	def __init__(self):
		self.TELEGRAM_BOT_TOKEN=environ.get('TELEGRAM_BOT_TOKEN')
		self.TELEGRAM_BOT_USERNAME=environ.get('TELEGRAM_BOT_USERNAME')
		self.FORTUNES_DIR=environ.get('FORTUNES_DIR')

