#!/usr/bin/env python

import os
import random
import codecs

DEFAUT_FORTUNES_DIR='/usr/share/games/fortunes'

class Fortune_header(object):
	STR_RANDOM=0b1
	STR_ORDERED=0b10
	STR_ROTATED=0b100

	def get_field(self, f, field, wsize=4):
		f.seek(wsize*field)
		ulong=f.read(wsize)
		return int.from_bytes(ulong, byteorder='big', signed=False)


	def __init__(self, filename):
		self.filename=filename

		with open(filename, 'rb') as index_file:
			self.STR_VERSION=self.get_field(index_file, 0)
			self.STR_NUMSTR=self.get_field(index_file, 1)
			self.STR_LONGLEN=self.get_field(index_file, 2)
			self.STR_SHORTLEN=self.get_field(index_file, 3)
			self.STR_FLAGS=self.get_field(index_file, 4)
			self.STR_DELIM=chr( self.get_field(index_file, 20,1) )

	def is_rotated(self):
		return ( self.STR_FLAGS & self.STR_ROTATED ) != 0
	def is_ordered(self):
		return ( self.STR_FLAGS & self.STR_ORDERED ) != 0
	def is_random(self):
		return ( self.STR_FLAGS & self.STR_RANDOM ) != 0

	def get_entry(self, field):
		header_offset=3
		field+=header_offset

		with open(self.filename, 'rb') as index_file:
			start=self.get_field(index_file, field, 8)
			size=self.get_field(index_file, field+1, 8)-start-2

			return start, size


		"""
unsigned long   str_version;    /* version number */
unsigned long   str_numstr;     /* # of strings in the file */
unsigned long   str_longlen;    /* length of longest string */
unsigned long   str_shortlen;   /* length of shortest string */
#define STR_RANDOM      0x1     /* randomized pointers */
#define STR_ORDERED     0x2     /* ordered pointers */
#define STR_ROTATED     0x4     /* rot-13'd text */
unsigned long   str_flags;      /* bit field for flags */
char str_delim;                 /* delimiting character */
"""


def is_valid_fortune_file(f):
	return os.path.exists(f) and os.path.isfile(f) and os.path.isfile(f+".dat")

def get_valid_fortune_from_dir(path):
	ret=[]
	for f in os.listdir(path):
		f=path+"/"+f
		if is_valid_fortune_file(f):
			ret.append(os.path.abspath(f))
	return ret

# Future
def fortune(**kwargs):
	# Set runtime
	all_files=kwargs.pop('all_files', False)
	# Not yet
	equal_size=kwargs.pop('equal_size', False)
	# Not yet
	print_list=kwargs.pop('print_list', False)
	# Not yet
	long_dictums=kwargs.pop('long_dictums', False)
	# Not yet
	match_regex=kwargs.pop('match_regex', False)
	offensive=kwargs.pop('offensive', False)
	# Not yet
	short=kwargs.pop('short', False)
	# Not yet
	ignore_match=kwargs.pop('ignore_match', False)
	# Not yet
	wait=kwargs.pop('wait', False)
	fortune_list=kwargs.pop('fortune_list', [])
	# Not yet
	fortune_percent_list=kwargs.pop('fortune_percent_list', None)
	fortunes_dir=kwargs.pop('fortunes_dir', DEFAUT_FORTUNES_DIR)

	# Determine list of valid fortunes

	fortune_files=[]

	if all_files and offensive and fortune_list:
		for f in fortune_list:
			if os.path.isdir(f):
				fortune_files+=get_valid_fortune_from_dir(f)
			elif is_valid_fortune_file(f):
				fortune_files+=os.path.abspath(f)
			elif os.path.isdir(fortunes_dir+"/"+f):
				fortune_files+=get_valid_fortune_from_dir(fortunes_dir+"/"+f)
			elif is_valid_fortune_file(fortunes_dir+"/"+f):
				fortune_files+=os.path.abspath(fortunes_dir+"/"+f)

		all_fortunes=[]
		for f in fortune_files:
			f+="-o"
			if is_valid_fortune_file(f):
				all_fortunes.append(f)
			elif is_valid_fortune_file(fortunes_dir+"/"+f):
				all_fortunes.append(fortunes_dir+"/"+f)

		fortune_files+=all_fortunes

	elif all_files and fortune_list:
		for f in fortune_list:
			if os.path.isdir(f):
				fortune_files+=get_valid_fortune_from_dir(f)
			elif is_valid_fortune_file(f):
				fortune_files+=os.path.abspath(f)
			elif os.path.isdir(fortunes_dir+"/"+f):
				fortune_files+=get_valid_fortune_from_dir(fortunes_dir+"/"+f)
			elif is_valid_fortune_file(fortunes_dir+"/"+f):
				fortune_files+=os.path.abspath(fortunes_dir+"/"+f)
	else:
		fortune_list.append(fortunes_dir)
		for f in fortune_list:
			if os.path.isdir(f):
				fortune_files+=get_valid_fortune_from_dir(f)
			elif is_valid_fortune_file(f):
				fortune_files+=os.path.abspath(f)
			elif os.path.isdir(fortunes_dir+"/"+f):
				fortune_files+=get_valid_fortune_from_dir(fortunes_dir+"/"+f)
			elif is_valid_fortune_file(fortunes_dir+"/"+f):
				fortune_files+=os.path.abspath(fortunes_dir+"/"+f)

	choice=random.choice(fortune_files)
	# Run %
	return fortune_from_file(choice)

def get_field(index_file, field, wsize=4):
	with open(index_file, 'rb') as f:
		f.seek(wsize*field)
		ulong=f.read(wsize)
		return int.from_bytes(ulong, byteorder='big', signed=False)

def fortune_from_file(fortune_file):
	index_file=fortune_file+".dat"

	if not os.path.exists(index_file):
		raise RuntimeError("File not found")

	header=Fortune_header(index_file)

	field=random.randint(0, header.STR_NUMSTR-1)

	start, size=header.get_entry(field)

	ret=""
	#print("count: {}, field: {}, start: {}, size: {}".format(count, field, start, size))
	with open(fortune_file, 'rb') as f:
		f.seek(start)
		ret=f.read(size).decode("utf-8")

	if header.is_rotated():
		return codecs.encode(ret, 'rot_13')
	return ret

if __name__ == '__main__':
	#print(fortune_from_file('fortunes/brasil'))
	#print(get_valid_fortune_from_dir('fortunes/'))
	print(fortune())
	#print(Fortune_header('fortunes/brasil.dat').STR_VERSION)
	#print(Fortune_header('fortunes/brasil.dat').is_rotated())
	#h=Fortune_header('/usr/share/games/fortunes/limerick-o.dat')
	#print(fortune_from_file('/usr/share/games/fortunes/limerick-o'))
	#print(h.is_rotated())
	#print(h.STR_FLAGS)

